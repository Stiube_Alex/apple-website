import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../components/Home.vue";
import Contact from "../components/Contact.vue";
import Cooking from "../components/Cooking.vue";
import Cider from "../components/Cider.vue";
import Bibliography from "../components/Bibliography.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact
  },
  {
    path: "/cider",
    name: "Cider",
    component: Cider
  },
  {
    path: "/cooking",
    name: "Cooking",
    component: Cooking
  },
  {
    path: "/bibliography",
    name: "Bibliography",
    component: Bibliography
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
